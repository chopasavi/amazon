package amazonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testDriver.TestDriver;

public class AmazonHomePage extends TestDriver {

    public AmazonHomePage() {
        PageFactory.initElements(TestDriver.driver, this);
    }

    @FindBy(id = "twotabsearchtextbox")
    public WebElement searchBox;

    @FindBy(id = "nav-search-submit-text")
    public WebElement searchSubmit;

    public void sendSearchBoxText(String searchText) {
        searchBox.click();
        searchBox.clear();
        searchBox.sendKeys(searchText);
        searchSubmit.click();
    }
}