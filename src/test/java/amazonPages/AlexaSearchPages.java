package amazonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testDriver.TestDriver;

import java.util.List;

public class AlexaSearchPages extends TestDriver {

    public AlexaSearchPages() {
        PageFactory.initElements(TestDriver.driver, this);
    }

    @FindBy(xpath = "//li[@class=\"a-normal\"][1]")
    public WebElement secondPage;

    @FindBy(xpath = "//a[@class=\"a-link-normal a-text-normal\"]")
    public List<WebElement> allFoundItems;

    public void clickSecondPage() {
        secondPage.click();
    }

    public void clickItem(int itemNumber) {
        allFoundItems.get(itemNumber-1).click();
    }
}