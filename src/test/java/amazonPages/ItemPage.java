package amazonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testDriver.TestDriver;

public class ItemPage extends TestDriver {

    public ItemPage() {
        PageFactory.initElements(TestDriver.driver, this);
    }

    @FindBy(xpath = "//span[@id='submit.add-to-cart-announce']")
    public WebElement availableForPurchase;

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    public WebElement addToCart;

    @FindBy(xpath = "//span[@class='a-button a-button-base abb-intl-decline']")
    public WebElement noThanks;

    @FindBy(xpath = "//h1[@class='a-size-medium a-text-bold']")
    public WebElement cartConfirmation;

    public String availableToPurchase() {
        return availableForPurchase.getText();
    }

    public void clickAddToCart(){
        addToCart.click();
    }

    public void noThanksClose(){
        noThanks.click();
    }

    public String confirmationText(){
        return cartConfirmation.getText();
    }

}
