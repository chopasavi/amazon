package testng;

import amazonPages.AlexaSearchPages;
import amazonPages.AmazonHomePage;
import amazonPages.ItemPage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import testDriver.TestDriver;

import java.util.concurrent.TimeUnit;

public class AmazonAlexaTest extends TestDriver {

    public AmazonHomePage amazonHomePage;
    public AlexaSearchPages alexaSearchPages;
    public ItemPage itemPage;

    @Test
    public void amazon() {
        amazonHomePage = new AmazonHomePage();
        alexaSearchPages = new AlexaSearchPages();
        itemPage = new ItemPage();

        driver.navigate().to("https://www.amazon.com/");
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(amazonHomePage.searchBox));
        amazonHomePage.sendSearchBoxText("Alexa");

        wait.until(ExpectedConditions.visibilityOf(alexaSearchPages.secondPage));
        alexaSearchPages.clickSecondPage();

        wait.until(ExpectedConditions.visibilityOfAllElements(alexaSearchPages.allFoundItems));
        alexaSearchPages.clickItem(3);

        try { // this try and catch block is used, to show if item is not available to purchase
            itemPage.availableForPurchase.isDisplayed();
            Assert.assertEquals(itemPage.availableToPurchase(), "Add to Cart");
            itemPage.clickAddToCart();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            try {//this try and catch block is used, in case of "No Thanks" button does not appears.
                itemPage.noThanks.isDisplayed();
                itemPage.noThanksClose();
            } catch (NoSuchElementException e) {
                System.out.println("No - No thanks button");
            }
            wait.until(ExpectedConditions.visibilityOf(itemPage.cartConfirmation));
            Assert.assertEquals(itemPage.confirmationText(), "Added to Cart");
        } catch (NoSuchElementException e) {
            System.out.println("Item is not available for purchase");
        }
    }
}